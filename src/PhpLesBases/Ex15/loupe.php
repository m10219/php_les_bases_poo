<?php

if (!file_exists($argv[1])) {
    exit;
}
$text = file_get_contents($argv[1]);
$regex = '/(?<=")(?:\s?).+?(?:\s?)(?=")|(?<=">).+?(?=<)|(?<=com>)(?:\s?).+?(?:\s?)(?=<img)/';
$arr2 = [];
if (preg_match_all($regex, $text, $arr1)) {
    foreach ($arr1[0] as $value) {
        array_push($arr2, strtoupper($value));
    }
    $newhtml = str_replace($arr1[0], $arr2, $text);
    echo $newhtml;
}
