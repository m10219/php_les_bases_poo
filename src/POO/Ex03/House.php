<?php

namespace App\POO\Ex03;

abstract class House
{
    abstract public function getHouseName();

    abstract public function getHouseMotto();

    abstract public function getHouseSeat();

    public function introduce()
    {
        echo 'House ' . $this->getHouseName() . ' of ' . $this->getHouseSeat() . ' : "' . $this->getHouseMotto() . '"' . "\n";

        // printf(
    //     "%s %s of %s : \"%s\"\n",
    //     static::INTRODUCE_HOUSE,
    //     $this->getHouseName(),
    //     $this->getHouseSeat(),
    //     $this->getHouseMotto()
    }
}
