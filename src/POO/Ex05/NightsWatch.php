<?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    public static $tab = [];

    public function recruit($warrior)
    {
        array_push(self::$tab, $warrior);

        return self::$tab;
    }

    public function fight()
    {
        foreach (self::$tab as $fighter) {
            if (method_exists($fighter, 'fight')) {
                $fighter->fight();
            }
        }
    }
}
