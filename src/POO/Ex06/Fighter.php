<?php

namespace App\POO\Ex06;

use App\POO\Ex05\IFighter;

abstract class Fighter implements IFighter
{
    public function __construct(string $xtype)
    {
        $this->xtype = $xtype;
    }

    public function returnType(): string
    {
        return $this->xtype;
    }
    // abstract public function fight(string $target = ''): void;
}
