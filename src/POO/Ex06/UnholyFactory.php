<?php

namespace App\POO\Ex06;

use App\POO\Ex05\IFighter;

class UnholyFactory
{
    public array $tab = [];

    public function absorb(IFighter $fighter)
    {
        if ($fighter instanceof Fighter) {
            $xtype = $fighter->returnType();

            if (!in_array($fighter, $this->tab)) {
                echo '(Factory absorbed a fighter of type ' . $xtype . ")\n";
                $this->tab[] = $fighter;
            } else {
                echo '(Factory already absorbed a fighter of type ' . $xtype . ")\n";
            }
        } else {
            echo "(Factory can't absorb this, it's not a fighter)\n";
        }

        return $this;
    }

    public function fabricate(string $fab)
    {
        $var1 = false;
        foreach ($this->tab as $value) {
            if ($value->xtype == $fab) {
                $var1 = true;
            }
        }
        if ($var1) {
            echo '(Factory fabricates a fighter of type ' . $fab . ")\n";
            print_r($this->tab);

            return $this->tab[$fab];
        } else {
            echo "(Factory hasn't absorbed any fighter of type " . $fab . ")\n";
        }
    }
}
