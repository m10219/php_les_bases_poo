<?php

namespace App\POO\Ex00;

use App\Resources\Classes\Lannister\Lannister;

class Tyrion extends Lannister
{
    public const SIZE = 'Short';

    protected function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT . 'My name is Tyrion' . "\n";
        }
    }

    public const EVENDRUNK = "Not even if I'm drunk !";
    public const LETSGO = "Let's do this.";

    public function sleepWith($name)
    {
        if (get_class($name) == 'App\Resources\Classes\Stark\Sansa') {
            echo self::LETSGO . "\n";
        } else {
            echo self::EVENDRUNK . "\n";
        }
    }
}
