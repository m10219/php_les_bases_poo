<?php

namespace App\POO\Ex04;

use App\Resources\Classes\Lannister\Lannister;

// use App\POO\Ex00\Tyrion;
// use App\Resources\Classes\Lannister\Cersei;
// use App\Resources\Classes\Stark\Sansa;

class Jaime extends Lannister
{
    public const EVENDRUNK = "Not even if I'm drunk !";
    public const LETSGO = "Let's do this.";
    public const OHYEAHSIS = 'With pleasure, but only in a tower in Winterfell, then.';

    public function sleepWith($name)
    {
        if (get_class($name) == 'App\Resources\Classes\Lannister\Cersei') {
            echo self::OHYEAHSIS . "\n";
        } elseif (get_class($name) == 'App\Resources\Classes\Stark\Sansa') {
            echo self::LETSGO . "\n";
        } else {
            echo self::EVENDRUNK . "\n";
        }
    }
}
