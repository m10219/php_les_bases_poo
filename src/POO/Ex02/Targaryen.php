<?php

namespace App\POO\Ex02;

class Targaryen
{
    public const NAKED = 'emerges naked but unharmed';
    public const ALIVE = 'burns alive';

    public function getBurned(): string
    {
        if (method_exists($this, 'resistsFire')) {
            return Targaryen::NAKED;
        } else {
            return Targaryen::ALIVE;
        }
    }
}
